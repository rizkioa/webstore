#!/bin/bash

if [ -f /etc/redhat-release ] ; then
	PKGMGR=`which yum`
elif [ -f /etc/debian_version ] ; then
	PKGMGR=`which apt-get`
else
	echo "Qual comando executa o gerenciador de pacotes da sua distro Linux? (apt-get, yum, pacman, emerge, ...)";
	read PKGMGR;
fi

echo "Fornecer a senha de \"root\" quando exigido, para instalar: Apache, MySQL e PHP...";
sudo $PKGMGR install mysql-server php5 php5-mysql apache2;

USER='web1';
PASS=$USER;
SQL_FILE='./DB/script_MySQL.sql';
DB='ecommerce';
MYSQL=`which mysql`;

echo "Dados do banco MySQL:";
echo "---------------------";
echo "Usuario: $USER";
echo "Senha  : $PASS (no banco, criptografada)";
echo "Banco  : $DB";
echo "Script : $SQL_FILE";

SQL="DROP DATABASE IF EXISTS ${DB};
CREATE DATABASE ${DB} CHARACTER SET utf8 COLLATE utf8_unicode_ci;
CREATE USER '${USER}'@'localhost';
SET PASSWORD FOR '${USER}'@'localhost' = password('${PASS}');
GRANT ALL PRIVILEGES ON ${DB}.* TO '${USER}'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;"

echo "Fornecer a senha de \"root\" para o MySQL quando solicitado:"
$MYSQL -u root -p -e "$SQL";
#echo "Fornecer sua senha de usuario $USER para o MySQL quando solicitado:"
#$MYSQL -u $USER -p -e "$SQL";
$MYSQL -u $USER -p$PASS -D $DB < $SQL_FILE;

echo "Banco \"$DB\" criado pelo usuario \"$USER\"";

echo "Fornecer a senha de \"root\" de for solicitado:";
sudo cp -rf ecommerce /var/www/.;
USER=`whoami`;
sudo chown -R $USER:$USER /var/www/ecommerce;

echo "Instalacao finalizada...!";

FIRE=`which firefox`;
$FIRE -new-window localhost/ecommerce &

exit;

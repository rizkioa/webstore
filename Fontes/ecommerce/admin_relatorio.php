<?php require ('cria_sessao.php');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
		<script type="text/javascript" src="./js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="./js/accordion.js" > </script>
		<title> WebStore | Admin</title>
		<link rel="stylesheet" type="text/css" href="./css/style.css" />
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<link rel="stylesheet" href="/resources/demos/style.css" />
		<link rel="stylesheet" href="./css/style_admin.css" />
		<link rel="stylesheet" href="./css/style_admin_relat.css" />
		<link rel="stylesheet" href="./css/style_produtos_imagens_temp.css" />
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script type="text/javascript" src="./js/jquery.maskedinput.min.js"></script>
		<script type="text/javascript" src="./js/validacoes.js"></script>
		<script type="text/javascript" src="./js/ajax.js"></script>
		<link rel="shortcut icon" href="./images/shoppingcart.png" />
		<script type="text/javascript">
			$(function() {
				$(".datepicker").mask("99/99/9999");
			});
			$(function() {
				$( "#tabs" ).tabs();
			});
		</script>
	</head>
	<body>
		<div id="barratop_adm">
			<img id="img_adm" src="./images/admin.png"/>
			<div id="title_adm"> WebStore Admin</div>
		</div>
		
		<?php include ("includes/menu_adm.inc"); ?>

		<div id="main_container_adm">
			<div id="main_content">
				<div id="tabs">
					<ul>
						<li><a href="#tabs-1" name="vendas_geral" class="menu_select">Vendas | Geral</a></li>
						<li><a href="#tabs-2" name="vendas_fabricante" class="menu_select">Vendas | Fabricante</a></li>
						<li><a href="#tabs-3" name="clientes_fieis" class="menu_select">Clientes | Fidelidade</a></li>
					</ul>
					<div id="tabs-1">
						<div class="fundo_admin_relat">
							<div id="grid_search_relat">
								<div class="search_data">Vendas entre</div>
								<form name="form_post" id="form_post" action="" method="POST" >
									<div class="search_data"><input type="text" id="inicial1" name="inicial1" class="datepicker"></div>
									<div class="search_data">e</div>
									<div class="search_data"><input type="text" name="final1" id="final1" class="datepicker"></div>
									<div class="search_data"><input type="button" onclick="vendas()"  value="Pesquisar"  class="search_buttom_crud" id="pesquisar"/></div>
								</form>
							</div>
							<div id="grid_menu_compras">
								<div id="grid_menu_compras1"> <span class="sub_menu_compras">Número da Compra - Comprador </span> </div>
								<div id="grid_menu_users2"> <span class="sub_menu_compras"> Data </span> </div>
								<div id="grid_menu_users3"> <span class="sub_menu_compras1"> </span> </div>
								<div id="grid_menu_compras4"> <span class="sub_menu_compras2"> Valor </span> </div>
							</div>
							<div id="fundo_compras" class="fundo_compras">
								
								<?php include ('alimenta_admin_relatorio_vendas.php'); ?>
							
							</div>
						</div>
					</div>
					<div id="tabs-2">
						<div class="fundo_admin_relat" >
							<div id="grid_search_relat">
								<div class="search_data">Vendas entre</div> 
								<form name="form_post" id="form_post" method="POST">
									<div class="search_data"><input type="text" id="inicial2" name="inicial2" class="datepicker"></div>
									<div class="search_data">e</div>
									<div class="search_data"><input type="text" name="final2" id="final2" class="datepicker"></div>
									<div class="search_data_r"><input type="button" onclick="fabricante()" class="search_buttom_crud" id="pesquisar" value="Pesquisar"/></div>
									<div class="search_data_r"><input type="text" name="fabric" id="fabric" class="cadastro_input_medium"/></div>
									<div class="search_data_r">ou Pesquise por fabricante:</div>
								</form>
							</div>
							<div id="grid_menu_compras">
								<div id="grid_menu_compras1"> <span class="sub_menu_compras"> Número da compra - Produto </span> </div>
								<div id="grid_menu_users2"> <span class="sub_menu_compras"> Data </span> </div>
								<div id="grid_menu_users3"> <span class="sub_menu_compras1"> Quantidade </span> </div>
								<div id="grid_menu_compras4"> <span class="sub_menu_compras2"> Valor </span> </div>
							</div>
							<div id="fundo_compras2" class="fundo_compras">
								
								<?php include ('alimenta_admin_fabricantes.php'); ?>
							
							</div>
						</div>
					</div>
					<div id="tabs-3">
						<div class="fundo_admin_relat">
							<div id="grid_search_relat">
								<div class="search_data">Pesquisar Clientes</div>
								<form name="form_post" id="form_post" method="POST" >
									<div class="search_data"><input type="text" name="form_cliente" id="form_cliente" class="cadastro_input_medium"/></div>
									<div class="search_data"><input type="button" name="clientes" class="search_buttom_crud" id="pesquisar" onclick="fidelidade()" value="pesquisar"/></div>
								</form>
							</div>
							<div id="grid_menu_compras">
								<div id="grid_menu_compras1"> <span class="sub_menu_compras">ID - Cliente </span> </div>
								<div id="grid_menu_users2"> <span class="sub_menu_compras"> </span> </div>
								<div id="grid_menu_users3"> <span class="sub_menu_compras1"> </span> </div>
								<div id="grid_menu_compras4"> <span class="sub_menu_compras2"> Total</span> </div>
							</div>
							<div id="fundo_compras3" class="fundo_compras">
							
								<?php include ('alimenta_admin_clientesfieis.php'); ?>
							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- end of main_container -->

		<div id="baixo">
			<div class="center_footer">
				Todos os Direitos Reservados - Designed by CSS Creme, editado pelo grupo ( Eduardo, Jhonny & Kleber ).<br /><br />
			</div>
		</div>
	</body>
</html>

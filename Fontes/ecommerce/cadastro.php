<?php
	require ('cria_sessao.php');

	if(isset($_SESSION["usuario"])):
		header("location: ./conta_cliente.php");
	else:
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>WebStore</title>
		<link rel="stylesheet" href="./css/style.css">
		<link rel="stylesheet" href="./css/style_produtos_imagens_temp.css">
		<link rel="stylesheet" type="text/css" href="./css/style_pagamento_img.css"/>
		<link rel="stylesheet" type="text/css" href="../ecommerce/css/style_pagina.css" media="screen"/>
		<link rel="shortcut icon" href="./images/shoppingcart.png" />
		<?php require ("./includes/js.inc"); ?>
	</head>
	<body>
		
		<?php include ("./includes/menu_superior.inc"); ?>
		
		<div id="main_container">
			<div id="presentation">
				<?php
					include ("./includes/slider.inc");
					include ("./includes/login.inc");
				?>
			</div>

			<div id="main_content">
				<div class="left_content">
					<?php include ("./includes/accordion_menu.inc"); ?>
				</div>	<!-- CLOSE: class="left_content" -->

				<div class="center_content">
					<div class="center_title_bar">Cadastro (Preencha os campos abaixo para realizar seu cadastro)
						<!--<div class="carrinho">Meu Carrinho
							<div id="interior_carrinho">
								<div id="fundo_interior_carrrinho"> </div>	
							</div>-->
						</div>	
					</div>
					<!--Formulario de Cadastro-->
					<div class="prod_box_big">
						<div class="center_prod_box_big">
							<div class="cadastro_form">
								<form name="cadastro" action="salva_cliente_no_banco.php?page=<?= $lastpage = basename($_SERVER['PHP_SELF']);?>" method="post" onsubmit='return validar_form(this);'>
									<div class="form_row">
										<label class="cadastro"><strong>Primeiro Nome:</strong></label>
										<input type="text" name="primeiro_nome" class="cadastro_input" maxlength="30"/>
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Último Nome:</strong></label>
										<input type="text" name="ultimo_nome" class="cadastro_input" maxlength="30"/>
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Apelido:</strong></label>
										<input type="text" name="usuario" class="cadastro_input" maxlength="30"/>
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Data de Nascimento:</strong></label>
										<input type="text" name="data_nascimento" id="date" class="datepicker">
										<label id="cadast_sex"><strong>Sexo:</strong></label>
											<label class="cadastro_sex"> <strong>M</strong> </label>
											<input type="radio" name="sexo" value="M">
											<label class="cadastro_sex"> <strong>F</strong> </label>
											<input type="radio" name="sexo" value="F">
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>CPF:</strong></label>
										<input type="text" id="cpf" name="cpf" class="cadastro_input" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Endereço:</strong></label>
										<input type="text" name="logradouro" class="cadastro_input_medium" maxlength="30"/>
										<label class="cadastro_short"><strong>N.º:</strong></label>
										<input type="text" name="numero"  id="numero" class="cadastro_input_short" maxlength="4"/>
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Complemento:</strong></label>
										<input type="text" name="complemento" class="cadastro_input_medium" maxlength="30"/>
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Bairro:</strong></label>
										<input type="text" name="bairro" class="cadastro_input_medium" />
									</div>	
									<div class="form_row">
										<label class="cadastro"><strong>CEP:</strong></label>
										<input type="text" id="cep" name="cep" class="cadastro_input_medium" />
									</div>
									<div class="form_row">
										<?php include ('./includes/busca_cidade.php'); ?>
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Telefone:</strong></label>
										<input type="text" name="telefone" id="telefone" class="cadastro_input" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>E-mail:</strong></label>
										<input type="email" name="semail" class="cadastro_input" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Confirmar E-mail:</strong></label>
										<input type="email" name="cemail" class="cadastro_input" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Senha:</strong></label>
										<input type="password" name="senha" class="cadastro_input" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Confirmar Senha:</strong></label>
										<input type="password" name="csenha" class="cadastro_input" />
									</div>
									<div class="form_row">
										<input type="submit" name="salvar" id="enviar" value="Salvar" />
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>	<!-- CLOSE: class="center_content" -->

				<div class="right_content">
					<div class="shopping_cart">

					</div>
				</div>	<!-- CLOSE: class="right_content" -->

			</div>	<!-- CLOSE: id="main_content" -->
			<div class="footer"></div> <!-- NÃO DELETAR PORQUE CARREGA CSS -->
		</div>	<!-- CLOSE: id="main_container" -->

		<?php include ("./includes/footer.inc"); ?>
		
	</body>
</html>
<?php
	endif;
?>

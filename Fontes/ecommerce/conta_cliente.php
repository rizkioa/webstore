<?php require ('cria_sessao.php');
	$lastpage = basename($_SERVER['PHP_SELF']); 
	if(isset($_SESSION["usuario"])){
		require ('alimenta_dados_cliente.php');
	}
	else{
		header("location: ./cadastro.php");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>WebStore</title>
		<link rel="stylesheet" href="./css/style.css">
		<link rel="stylesheet" href="./css/style_produtos_imagens_temp.css">
		<link rel="stylesheet" type="text/css" href="./css/style_pagamento_img.css"/>
		<link rel="stylesheet" href="/resources/demos/style.css" />
		<link rel="shortcut icon" href="./images/shoppingcart.png" />
		<?php require ("./includes/js.inc"); ?>
	</head>
	<body>
		
		<?php include ("./includes/menu_superior.inc"); ?>
		
		<div id="main_container">
			<div id="presentation">
				<?php
					include ("./includes/slider.inc");
					include ("./includes/login.inc");
				?>
			</div>

			<div id="main_content">
				<div class="left_content">
					<?php include ("./includes/accordion_menu.inc"); ?>
				</div>	<!-- CLOSE: class="left_content" -->

				<div class="center_content">
					<div class="center_title_bar"> Meu Cadastro (Atualize seu cadastro)
						<!--<div class="carrinho">Meu Carrinho
							<div id="interior_carrinho">
								<div id="fundo_interior_carrrinho"> </div>	
							</div>-->
						</div>	
					</div>
					<!--Formulario de Cadastro-->
					<div class="prod_box_big">
						<div class="center_prod_box_big">
							<div class="cadastro_form">
								<form name="cadastro" action="atualiza_cliente_no_banco.php" method="post" onsubmit='return validar_form(this);'>
									<div class="form_row">
										<label class="cadastro"><strong>Primeiro Nome:</strong></label>
										<input type="text" name="primeiro_nome" class="cadastro_input" value="<?= $linha[primeiro_nome] ?>" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Último Nome:</strong></label>
										<input type="text" name="ultimo_nome" class="cadastro_input" value="<?= $linha[ultimo_nome] ?>" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Apelido:</strong></label>
										<input type="text" name="usuario" class="cadastro_input" value="<?= $linha[usuario] ?>"/>
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Data de Nascimento:</strong></label>
										<input type="text" id="date" name="data_nascimento" id="datepicker" class="datepicker" value="<?= $date ?>">
										<label id="cadast_sex"><strong>Sexo:</strong></label>
										<?php if($linha[sexo] == 'M'){
											?>	
												<label class="cadastro_sex"> <strong>M</strong> </label>
												<input type="radio" name="sexo" checked="checked" value="M">
												<label class="cadastro_sex"> <strong>F</strong> </label>
												<input type="radio" name="sexo" value="F">
											<?php
											}
											elseif($linha[sexo]=='F'){ ?>
												<label class="cadastro_sex"> <strong>M</strong> </label>
												<input type="radio" name="sexo" value="M">
												<label class="cadastro_sex"> <strong>F</strong> </label>
												<input type="radio" name="sexo" value="F" checked="checked">												
												<?php					
											}
										?>
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>CPF:</strong></label>
										<input type="text" id="cpf" name="cpf" class="cadastro_input" value="<?= $linha[CPF] ?>" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Endereço:</strong></label>
										<input type="text" name="logradouro" class="cadastro_input_medium" value="<?= $linha[logradouro] ?>" />
										<label class="cadastro_short"><strong>N.º:</strong></label>
										<input type="text" id="numero" name="numero" class="cadastro_input_short" value="<?= $linha[numero] ?>" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Complemento:</strong></label>
										<input type="text" name="complemento" class="cadastro_input_medium" value="<?= $linha[complemento] ?>" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Bairro:</strong></label>
										<input type="text" name="bairro" class="cadastro_input_medium" value="<?= $linha[bairro] ?>" />
									</div>	
									<div class="form_row">
										<label class="cadastro"><strong>CEP:</strong></label>
										<input type="text" id="cep" name="cep" class="cadastro_input_medium" value="<?= $linha[CEP] ?>" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Cidade:</strong></label>
										<input type="text" name="cidade" class="cadastro_input_medium"  value="<?= $consulta[nome_cidade] ?>" />
									</div>
									<div class="form_row">
										<input type="hidden" name="id" value="<?= $linha[cod_usuario]; ?>" >
										<input type="hidden" name="lastpage" value="<?= $lastpage = basename($_SERVER['PHP_SELF']) ?>" >
										<input type="submit" name="salvar" id="enviar" value="Atualizar" />
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>	<!-- CLOSE: class="center_content" -->

				<div class="right_content">
					<div class="shopping_cart">

					</div>
				</div>	<!-- CLOSE: class="right_content" -->

			</div>	<!-- CLOSE: id="main_content" -->
			<div class="footer"></div> <!-- NÃO DELETAR PORQUE CARREGA CSS -->
		</div>	<!-- CLOSE: id="main_container" -->

		<?php include ("./includes/footer.inc"); ?>
		
	</body>
</html>


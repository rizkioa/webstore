<?php require ('cria_sessao.php'); ?>
<?include ('finaliza_compra.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>WebStore</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/style_pagamento_img.css" />
		<link rel="stylesheet" type="text/css" href="css/style_pagamento_formas.css" />
		<link rel="stylesheet" type="text/css" href="css/style_carrinho.css" />
		<link rel="shortcut icon" href="images/shoppingcart.png" />
		<?php require ("./includes/js.inc"); ?>
	</head>
	<body>

		<?php include ("includes/menu_superior.inc"); ?>
		
		<div id="main_container">
			<div id="presentation">
				<?php
					include ("./includes/slider.inc");
					include ("./includes/login.inc");
				?>
			</div>
		<div id="main_content">
			<div class="barra_produto">
				<div id="carrinho_menu_text">Carrinho de Compras</div> 
				<div id="carrinho_img"></div>	
			</div>	
			
			<div id="status_container">
				<div id="status_ok"> </div>
				<div class="text_info" > <span> Carrinho de Compras </span> </div>
			</div>
		
			<div class="status_flecha"><img src="images/flecha.png"/></div>
		
			<div id="status_container">
				<div id="status_ok"> </div>
				<div class="text_info" > <span>Identificação</span> </div>
			</div>
			
			<div class="status_flecha"><img src="images/flecha.png"/></div>
			
			<div id="status_container">
				<div id="status_i"> </div>
				<?php if($com == 0){
					   ?><div class="text_info" > <span>Pagamento</span> </div><?php
 					}
					else{ ?>
						<div class="text_info" > <span>Verificando Pagamento</span> </div><?php
					}?>
			</div>
			
			<div class="status_flecha"><img src="images/flecha.png"/></div>
			
			<div id="status_container">
				<div id="status_i"> </div>
				<div class="text_info" > <span>Confirmação / Envio</span> </div>
			</div>
			

		<div id="grid_fin_compra_cabecalho">
			<div id="submenu_cabecalho_pedido"><span class="submenu_grid_compra">Produto</span></div>
			<div id="submenu_cabecalho_qtd"><span class="submenu_grid_compra">Quantidade</span></div>
			<div id="submenu_cabecalho_qtd"><span class="submenu_grid_compra">Valor Unit.</span></div>
			<div id="submenu_cabecalho_qtd"><span class="submenu_grid_compra">Subtotal</span></div>
		</div>
		<div id="grid_fin_compra">
		<?php
			include 'alimenta_pagamento.php';
		?>
		</div>
		<form name="paga" action="finaliza_compra.php" method="post" onsubmit='return compra(this);'>
			<div class="barra_title_pagamento">
				<span class="text_escolha_pagamento"> Cartão de Crédito </span>
				<span class="text_escolha_pagamento_aux"> <i>( Mais rápido e parcelável )</i> </span>
			</div>
			<div class="corpo_pagamento_options">
				<div class="logo_pagamento_radio">
					<input type="radio" name="pagemento" value="master" />
					<div id="logo_pagamento_img_1"> </div> <br />
				</div>
				
				<div class="logo_pagamento_radio">
					<input type="radio" name="pagemento" value="visa" />
					<div id="logo_pagamento_img_2"> </div> <br />
				</div>
				
				<div class="logo_pagamento_radio">
					<input type="radio" name="pagemento" value="hiper" />
					<div id="logo_pagamento_img_3"> </div> <br />
				</div>
				
				<div class="logo_pagamento_radio">
					<input type="radio" name="pagemento" value="diner" />
					<div id="logo_pagamento_img_4"> </div> <br />
				</div>		
			</div>
			
			<div class="barra_title_pagamento">
				<span class="text_escolha_pagamento"> Transferência Bancária </span>
				<span class="text_escolha_pagamento_aux"> <i>( Débito à vista em conta )</i> </span>
			</div>
			<div class="corpo_pagamento_options">
				<div class="logo_pagamento_radio">
					<input type="radio" name="pagemento" value="bb" />
					<div id="logo_pagamento_img_5"> </div> <br />
				</div>
				
				<div class="logo_pagamento_radio">
					<input type="radio" name="pagemento" value="bradesco" />
					<div id="logo_pagamento_img_6"> </div> <br />
				</div>
				
				<div class="logo_pagamento_radio">
					<input type="radio" name="pagemento" value="itau" />
					<div id="logo_pagamento_img_7"> </div> <br />
				</div>
				
				<div class="logo_pagamento_radio">
					<input type="radio" name="pagemento" value="real" />
					<div id="logo_pagamento_img_8"> </div> <br />
				</div>		
			</div>
			
			<div class="barra_title_pagamento">
				<span class="text_escolha_pagamento"> Boleto Bancário </span>
				<span class="text_escolha_pagamento_aux"> <i>( 10% de desconto )</i> </span>
			</div>
			<div class="corpo_pagamento_options">
				<div class="logo_pagamento_radio">
					<input type="radio" name="pagemento" value="boleto" />
					<div id="logo_pagamento_img_9"> </div> <br />
				</div>
				<input type="submit" id="enviar" href="../ecommerce/images/gera_boleto.png" target="blank" onclick="window.open(this.href, this.target, 'width=754,height=479'); return false;"
					value="Gerar Boleto"/>
			</div>
	
			<div class="barra_title_pagamento"> <!-- dados do cartão de crédito -->
				<span class="text_escolha_pagamento"> Insira os dados do cartão </span>
				<span class="text_escolha_pagamento_aux"> <i>( Apenas para Crédito e Transferência )</i> </span>
			</div>
			<div class="corpo_pagamento_dados">
				<span class="text_dados_cartao"><i>Número do Cartão:</i></span><input name="compra1"class="dados_cartao_2" type="integer" size="30" /> <br />
				<span class="text_dados_cartao"><i>Nome do Titular no Cartão:</i></span> <input name="compra2" class="dados_cartao" type="integer" size="30" /> <br />
				<span class="text_dados_cartao"><i>CPF do Titular:</i></span> <input name="compra3" class="dados_cartao_3" id="cpf" type="integer" size="30" /> <br />
				<span class="text_dados_cartao"><i>Código de Segurança:</i></span> <input name="compra4" class="dados_cartao" id="cod_seg" type="integer" size="2" />
				<span class="text_dados_cartao"><i>Validade:</i></span> 
				<select name="dropdown_mes" id="dropdown_mes">
									<option value="1"> 01 </option>
									<option value="2"> 02 </option>
									<option value="3"> 03 </option>
									<option value="4"> 04 </option>
									<option value="5"> 05 </option>
									<option value="6"> 06 </option>
									<option value="7"> 07 </option>
									<option value="8"> 08 </option>
									<option value="9"> 09 </option>
									<option value="10"> 10 </option>
									<option value="11"> 11 </option>
									<option value="12"> 12 </option>
							</select>
							
							<select name="dropdown_ano" id="dropdown_ano">
									<option value="1"> 2013 </option>
									<option value="2"> 2014 </option>
									<option value="3"> 2015 </option>
									<option value="4"> 2016 </option>
									<option value="5"> 2017 </option>
									<option value="6"> 2018 </option>
									<option value="7"> 2019 </option>
									<option value="8"> 2020 </option>
							</select> <br />
				<span class="text_dados_cartao"><i>Quantidade de parcelas:</i></span> <i> (apenas para opção Crédito) </i>
				<select name="dropdown_qtd" id="dropdown_mes">
									<option value="1"> 01 </option>
									<option value="2"> 02 </option>
									<option value="3"> 03 </option>
									<option value="4"> 04 </option>
									<option value="5"> 05 </option>
									<option value="6"> 06 </option>
									<option value="7"> 07 </option>
									<option value="8"> 08 </option>
									<option value="9"> 09 </option>
									<option value="10">10 </option>
									<option value="11">11 </option>
									<option value="12">12 </option>
							</select>
				<span class="text_dados_cartao"><i>Total:</i> </span>
				<span class="text_qtd_valor"><i>R$ <?= $valortotal ?></i> </span>
				<input type="submit" id="enviar" value="Continuar"/>
			</form>
	</div> <!-- end dados do cartão de crédito -->

<!-- end of center pagamento-->
				
				
	</div><!-- end of main content -->
</div>
<!-- end of main_container -->
		<div class="footer"></div>

		<?php include ("includes/footer.inc"); ?>
	</body>
</html>

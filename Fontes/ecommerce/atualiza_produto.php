<?php require ('cria_sessao.php');
	  require 'dados_produto.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="./js/accordion.js" > </script>
		<title> WebStore | Admin</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<link rel="stylesheet" href="/resources/demos/style.css" />
		<link rel="stylesheet" type="text/css" href="./css/style.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_admin.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_admin_relat.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_pagamento_img.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_produtos_imagens_temp.css" />
		<link rel="shortcut icon" href="./images/shoppingcart.png" />
		<script>
			$(function() {
				$( "#tabs" ).tabs();
			});
		</script>
	</head>
	<body>
		<div id="barratop_adm">
			<img id="img_adm" src="./images/admin.png"/>
			<div id="title_adm"> WebStore Admin</div>
		</div>
		
		<?php include ("includes/menu_adm.inc"); ?>

		<div id="main_container_adm">
			<div id="main_content">
				<div id="tabs-3">
							<div id="fundo_admin_aux">
								<div id="fundo_admin2_aux">
									<div class="barra_add_remove_admin"> Atualizar Produto </div>
								
									  <form name="cadastro_prod" action="atualiza_produto_no_banco.php" method="post" enctype="multipart/form-data">
											<div class="form_row">
												<label class="cadastro"><strong>Nome:</strong></label>
												<input type="text" name="nome" class="cadastro_input" value="<?= $linha[nome]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Peso bruto:</strong></label>
												<input type="text" name="peso_b" class="cadastro_input_medium" value="<?= $linha[peso_bruto]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Peso liquido:</strong></label>
												<input type="text" name="peso_l" class="cadastro_input_medium" value="<?= $linha[peso_liquido]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Valor:</strong></label>
												<input type="text" name="valor" class="cadastro_input_medium" value="<?= $linha[valor]; ?>" />
											</div>															
											<div class="form_row">
												<label class="cadastro"><strong>Modelo:</strong></label>
												<input type="text" name="modelo" class="cadastro_input_medium" value="<?= $linha[modelo]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Marca:</strong></label>
												<input type="text" name="marca" class="cadastro_input_medium" value="<?= $marca[nome]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Câmera frontal:</strong></label>
												<input type="text" name="camera_frontal" class="cadastro_input_medium" value="<?= $linha[camera_frontal]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Tecnologia:</strong></label>
												<input type="text" name="tecnologia" class="cadastro_input_medium" value="<?= $linha[tecnologia]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Câmera traseira:</strong></label>
												<input type="text" name="camera_traseira" class="cadastro_input_medium" value="<?= $linha[camera_traseira]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Display:</strong></label>
												<input type="text" name="display" class="cadastro_input_medium" value="<?= $linha[display]; ?>" />
											</div>
															
											<div class="form_row">
												<label class="cadastro"><strong>Wi-fi:</strong></label>
												<input type="text" name="wifi" class="cadastro_input_medium" value="<?= $linha[wifi]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Bluetooth:</strong></label>
												<input type="text" name="bluetooth" class="cadastro_input_medium" value="<?= $linha[bluetooth]; ?>" />
											</div>
																		
											<div class="form_row">
												<label class="cadastro"><strong>Processador:</strong></label>
												<input type="text" name="processador" class="cadastro_input" value="<?= $linha[processador]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Memória Ram:</strong></label>
												<input type="text" name="memoria_ram" class="cadastro_input" value="<?= $linha[memoria_RAM]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Memória Interna:</strong></label>
												<input type="text" name="memoria_interna" class="cadastro_input" value="<?= $linha[memoria_interna]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Cor:</strong></label>
												<input type="text" name="cor" class="cadastro_input_medium" value="<?= $linha[cor]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Medidas LxAxP:</strong></label>
												<input type="text" name="medidas" class="cadastro_input_medium" value="<?= $linha[LxAxP]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Acompanha:</strong></label>
												<input type="text" name="acompanha" class="cadastro_input_medium" value="<?= $linha[acompanha]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Garantia:</strong></label>
												<input type="text" name="garantia" class="cadastro_input_medium" value="<?= $linha[garantia]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Recursos:</strong></label>
												<input type="text" name="recursos" class="cadastro_input" value="<?= $linha[recursos]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Quantidade:</strong></label>
												<input type="number" name="quantidade" class="cadastro_input" value="<?= $linha[quantidade]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Visível:</strong></label>
												<?php if($linha[visivel]){ ?>
												<input type="checkbox" name="aparecer" class="cadastro_input" checked="checked" />
												<?php } else{ ?>
												<input type="checkbox" name="aparecer" class="cadastro_input" />
												<?php } ?>
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Desconto (em %):</strong></label>
												<input type="text" name="desconto" class="cadastro_input_medium" value="<?= $linha[desconto]; ?>" />
											</div>
											
											<div class="form_row">
												<label class="cadastro"><strong>Idiomas:</strong></label>
												<input type="text" name="idioma" class="cadastro_input" value="<?= $linha[idiomas]; ?>" />
											</div>		
											
											<div class="form_row">
												<label class="cadastro"><strong>Imagens:</strong></label>
												<input type="file" name="imagem" multiple="multiple" />
												<input type="hidden" name="id" value="<?= $linha[cod_produto] ?>" />
												<input type="submit" value="Atualizar" id="salvar_form" />
											</div>								
										</form>
											
									  </div>
								</div>
					</div>
				</div>
		</div><!-- end of main_container_adm-->

		<div id="baixo">
			<div class="center_footer">
				Todos os Direitos Reservados - Designed by CSS Creme, editado pelo grupo ( Eduardo, Jhonny & Kleber ).<br /><br />
			</div>
		</div>
	</body>
</html>

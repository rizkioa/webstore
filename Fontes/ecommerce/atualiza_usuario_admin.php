<?php require ('cria_sessao.php');
      require ('alimenta_dados_cliente.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="./js/accordion.js" > </script>
		<title> WebStore | Admin</title>
		<link rel="stylesheet" type="text/css" href="./css/style.css" />
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<link rel="stylesheet" href="/resources/demos/style.css" />
		<link rel="stylesheet" type="text/css" href="./css/style.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_admin.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_admin_relat.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_pagamento_img.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_produtos_imagens_temp.css" />
		<link rel="shortcut icon" href="./images/shoppingcart.png" />
		<script type="text/javascript" src="./js/validacoes.js"></script>
		<script type="text/javascript" src="./js/jquery.maskedinput.min.js"></script>
		<script>
			$(function() {
				$("#date").mask("99/99/9999");
				$("#cep").mask("99.999-999");
				$("#cpf").mask("999.999.999-99");
				$("#telefone").mask("(99)9999-9999?9");
				$("#numero").mask("9?99999");
			});
			$(function() {
				$( "#tabs" ).tabs();
			});
		</script>
	</head>
	<body>
		<div id="barratop_adm">
			<img id="img_adm" src="./images/admin.png"/>
			<div id="title_adm"> WebStore Admin</div>
		</div>
		
		<?php include ("includes/menu_adm.inc"); ?>

		<div id="main_container_adm">
			<div id="main_content">
				<div id="tabs">
					<div id="tabs-2">
						<div id="fundo_admin">
							<div id="fundo_admin2">
								<div class="barra_add_remove_admin"> Atualizar usuário </div>
								<form name="cadastro" action="atualiza_cliente_no_banco.php" method="post" onsubmit='return validar_form(this);'>
									<div class="form_row">
										<label class="cadastro"><strong>Administrador:</strong></label>
										<?php if($linha[admin]==1){ ?>
										<input type="checkbox" name="admin" checked="checked" /> <?php } else { ?>
										<input type="checkbox" name="admin"/> <?php } ?> 
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Primeiro Nome:</strong></label>
										<input type="text" name="primeiro_nome" class="cadastro_input" maxlength="30" value="<?= $linha[primeiro_nome] ?>" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Último Nome:</strong></label>
										<input type="text" name="ultimo_nome" class="cadastro_input" maxlength="30" value="<?= $linha[ultimo_nome] ?>" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Apelido:</strong></label>
										<input type="text" name="usuario" class="cadastro_input" maxlength="30" value="<?= $linha[usuario] ?>" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Data de Nascimento:</strong></label>
										<input type="text" id="date" name="data_nascimento" id="datepicker" class="datepicker" value="<?= $date ?>">
										<label id="cadast_sex"><strong>Sexo:</strong></label>
										<?php if($linha[sexo] == 'M') { ?>
											<label class="cadastro_sex"> <strong>M</strong> </label>
											<input type="radio" name="sexo" checked="checked" value="M">
											<label class="cadastro_sex"> <strong>F</strong> </label>
											<input type="radio" name="sexo" value="F">
										<?php
										}
										elseif($linha[sexo] == 'F') { ?>
											<label class="cadastro_sex"> <strong>M</strong> </label>
											<input type="radio" name="sexo" value="M">
											<label class="cadastro_sex"> <strong>F</strong> </label>
											<input type="radio" name="sexo" value="F" checked="checked">
										<?php
										}
										?>
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>CPF:</strong></label>
										<input type="text" id="cpf" name="cpf" class="cadastro_input" value="<?= $linha[CPF] ?>" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Endereço:</strong></label>
										<input type="text" name="logradouro" class="cadastro_input_medium" maxlength="30" value="<?= $linha[logradouro] ?>" />
										<label class="cadastro_short"><strong>N.º:</strong></label>
										<input type="text" name="numero"  id="numero" class="cadastro_input_short" maxlength="4" value="<?= $linha[numero] ?>" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Complemento:</strong></label>
										<input type="text" name="complemento" class="cadastro_input_medium" maxlength="30" value="<?= $linha[complemento] ?>" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Bairro:</strong></label>
										<input type="text" name="bairro" class="cadastro_input_medium" value="<?= $linha[bairro] ?>" />
									</div>	
									<div class="form_row">
										<label class="cadastro"><strong>CEP:</strong></label>
										<input type="text" id="cep" name="cep" class="cadastro_input_medium" value="<?= $linha[CEP] ?>" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Cidade:</strong></label>
										<input type="text" name="cidade" class="cadastro_input_medium"  value="<?= $consulta[nome_cidade] ?>" />
									</div>
									
									<div class="form_row">
										<input type="hidden" name="id" value="<?= $linha[cod_usuario]; ?>" >
										<input type="hidden" name="lastpage" value="<?= $lastpage = basename($_SERVER['PHP_SELF']) ?>" >
										<input type="submit" name="salvar" id="enviar" value="Atualizar" />
									</div>
								</form>
							</div>
						</div>
							
						</div>
					</div>
				</div>
			</div>
		</div><!-- end of main_container_adm-->

		<div id="baixo">
			<div class="center_footer">
				Todos os Direitos Reservados - Designed by CSS Creme, editado pelo grupo ( Eduardo, Jhonny & Kleber ).<br /><br />
			</div>
		</div>
	</body>
</html>

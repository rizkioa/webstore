<?php require 'cria_sessao.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>WebStore</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/style_pagamento_img.css" />
		<link rel="stylesheet" type="text/css" href="css/style_pagamento_formas.css" />
		<link rel="stylesheet" type="text/css" href="css/style_carrinho.css" />
		<link rel="shortcut icon" href="images/shoppingcart.png" />
		<link rel="stylesheet" type="text/css" href="css/style_produtos_imagens_temp.css" />
		<?php require ("./includes/js.inc"); ?>
	</head>
	<body>
		
		<?php include ("./includes/menu_superior.inc"); ?>
		
		<div id="main_container">
			<div id="presentation">
				<?php
					include ("./includes/slider.inc");
					include ("./includes/login.inc");
				?>
			</div>
			<div id="main_content">
				<div class="barra_produto">
					<div id="carrinho_menu_text">Carrinho de Compras
						<?php
							if($_GET['status']=='login'){
								echo "- Faça login para continuar a compra!";
							}
						?>	
					</div> 
					<div id="carrinho_img"></div>	
				</div>			
				<div id="status_container">
					<?php if(!count($_SESSION['carrinho'])){
					   ?>	<div id="status_x"> </div> <?php
 					}
					else{ ?>
						<div id="status_ok"> </div> <?php
					}?>
					<div class="text_info" > <span> Carrinho de Compras </span> </div>
				</div>
		
				<div class="status_flecha"><img src="images/flecha.png"/></div>
		
				<div id="status_container">
					<?php if(!isset($_SESSION['usuario'])){
					   ?>	<div id="status_x"> </div> <?php
 					}
					else{ ?>
						<div id="status_ok"> </div> <?php
					}?>
					<div class="text_info" > <span>Identificação</span> </div>
				</div>
			
				<div class="status_flecha"><img src="images/flecha.png"/></div>
			
				<div id="status_container">
					<div id="status_i"> </div>
					<div class="text_info" > <span>Pagamento</span> </div>
				</div>
			
				<div class="status_flecha"><img src="images/flecha.png"/></div>
			
				<div id="status_container">
					<div id="status_i"> </div>
					<div class="text_info" > <span>Confirmação de Pagamento</span> </div>
				</div>
				<div id="box_big_carrinho_car">
					<div id="grid_menu_compras_car">
						<div id="grid_menu_compras1_car"> <span class="sub_menu_compras_car"> Produto </span> </div>
						<div id="grid_menu_compras2_car"> <span class="sub_menu_compras_car"> Quantidade </span> </div>
						<div id="grid_menu_compras3_car"> <span class="sub_menu_compras1_car"> Valor unitário </span> </div>
						<div id="grid_menu_compras3_car"> <span class="sub_menu_compras2_car"> Subtotal </span> </div>
						<div id="grid_menu_compras0_car"> <span class="sub_menu_compras_car"> Eliminar </span> </div>
					</div>
					
						<div id="fundo_compras_car">
							<?php include ('./lista_dados_carrinho.php');?>
						</div>
					
					<div id="balao_valor_total_car"> <?php $_SESSION['valorcompra'] = $valortotalaux ; $_SESSION['peso_b'] = $peso_b;
						$_SESSION['peso_l'] = $peso_l; ?>
						<span id="total_text_car"> <?= $valoraux = number_format($valortotalaux, 2, ',' , '.') ?> </span>
					</div>
					<form name="" action="valida_compra.php?&page=<?= $lastpage = basename($_SERVER['PHP_SELF']);?>" method="post">
					<?php
					if($_GET['status']=='login'){ ?>
						<input disabled="disabled" type="submit" id="enviar" value="Continuar"/> <?php } else { ?>
						<input type="submit" id="enviar" value="Continuar"/> <?php
						}
					?>
					</form>
				</div><!-- end of center pagamento-->
			</div><!-- end of main content -->
		</div>
		<!-- end of main_container -->

		<?php include ("./includes/footer.inc");?>
	</body>
	
</html>

<?php require ('cria_sessao.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="./js/accordion.js" > </script>
		<title> WebStore | Admin</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<link rel="stylesheet" href="/resources/demos/style.css" />
		<link rel="stylesheet" type="text/css" href="./css/style.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_admin.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_admin_relat.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_pagamento_img.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_produtos_imagens_temp.css" />
		<script type="text/javascript" src="./js/ajax.js"></script>
		<link rel="shortcut icon" href="./images/shoppingcart.png" />
		<script>
			$(function() {
				$( "#tabs" ).tabs();
			});
		</script>
	</head>
	<body>
		<div id="barratop_adm">
			<img id="img_adm" src="./images/admin.png"/>
			<div id="title_adm"> WebStore Admin</div>
		</div>
		
		<?php include ("includes/menu_adm.inc"); ?>

		<div id="main_container_adm">
			<div id="main_content">
				<div id="tabs">
					<ul>
						<li><a href="#tabs-1" class="menu_select"> Produtos | Geral</a></li>
						<li><a href="#tabs-2" class="menu_select"> Adicionar </a></li>
					</ul>
					<div id="tabs-1">
						<div id="fundo_admin">
							<div id="grid_search_relat">
								<div class="search_data">Pesquisar produtos</div>
								<form method="POST" name="form_post" id="form_post" >
									<div class="search_data">
										<input name="search_produto" id="search_produto" type="text" class="cadastro_input_medium" />
									</div>
									<div class="search_data">
										<button type="button" onclick="acao5()" class="search_buttom_crud" id="pesquisar"/>Pesquisar</button>
									</div>
								</form>
							</div>
							<div id="grid_menu_compras">
								<div id="grid_menu_compras1"> <span class="sub_menu_compras"> ID - Produto (clique no modelo para atualizar informações)</span> </div>
								<div id="grid_menu_users2"> <span class="sub_menu_compras1"> Visível </span> </div>
								<div id="grid_menu_users3"> <span class="sub_menu_compras1"> Quantidade </span> </div>
								<div id="grid_menu_compras4"> <span class="sub_menu_compras2"> Desconto (%) </span> </div>
							</div>
							<div class="fundo_compras" id="fundo_produtos1">
								
								<?php include ('alimenta_admin_produtos.php'); ?>
								
							</div>
						</div>
					</div>
					<div id="tabs-2">
						<div id="fundo_admin_aux">
							<div id="fundo_admin2_aux">
								<div class="barra_add_remove_admin"> Adicionar Produto </div>
							
								  <form name="cadastro_prod" action="cadastro_produtos.php" method="post" enctype="multipart/form-data">
										<div class="form_row">
											<label class="cadastro"><strong>Nome:</strong></label>
											<input type="text" name="nome" class="cadastro_input" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Peso bruto:</strong></label>
											<input type="text" name="peso_b" class="cadastro_input_medium" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Peso liquido:</strong></label>
											<input type="text" name="peso_l" class="cadastro_input_medium" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Valor:</strong></label>
											<input type="text" name="valor" class="cadastro_input_medium" />
										</div>															
										<div class="form_row">
											<label class="cadastro"><strong>Modelo:</strong></label>
											<input type="text" name="modelo" class="cadastro_input_medium" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Marca:</strong></label>
											<input type="text" name="marca" class="cadastro_input_medium" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Câmera frontal:</strong></label>
											<input type="text" name="camera_frontal" class="cadastro_input_medium" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Tecnologia:</strong></label>
											<input type="text" name="tecnologia" class="cadastro_input_medium" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Câmera traseira:</strong></label>
											<input type="text" name="camera_traseira" class="cadastro_input_medium" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Display:</strong></label>
											<input type="text" name="display" class="cadastro_input_medium" />
										</div>
														
										<div class="form_row">
											<label class="cadastro"><strong>Wi-fi:</strong></label>
											<input type="text" name="wifi" class="cadastro_input_medium" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Bluetooth:</strong></label>
											<input type="text" name="bluetooth" class="cadastro_input_medium" />
										</div>
																	
										<div class="form_row">
											<label class="cadastro"><strong>Processador:</strong></label>
											<input type="text" name="processador" class="cadastro_input" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Memória Ram:</strong></label>
											<input type="text" name="memoria_ram" class="cadastro_input" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Memória Interna:</strong></label>
											<input type="text" name="memoria_interna" class="cadastro_input" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Cor:</strong></label>
											<input type="text" name="cor" class="cadastro_input_medium" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Medidas LxAxP:</strong></label>
											<input type="text" name="medidas" class="cadastro_input_medium" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Acompanha:</strong></label>
											<input type="text" name="acompanha" class="cadastro_input_medium" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Garantia:</strong></label>
											<input type="text" name="garantia" class="cadastro_input_medium" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Recursos:</strong></label>
											<input type="text" name="recursos" class="cadastro_input" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Quantidade:</strong></label>
											<input type="number" name="quantidade" class="cadastro_input" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Visível:</strong></label>
											<input type="checkbox" name="visivel" class="cadastro_input" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Desconto (em %):</strong></label>
											<input type="text" name="desconto" class="cadastro_input_medium" />
										</div>
										
										<div class="form_row">
											<label class="cadastro"><strong>Idiomas:</strong></label>
											<input type="text" name="idioma" class="cadastro_input" />
										</div>		
										
										<div class="form_row">
											<label class="cadastro"><strong>Imagem:</strong></label>
											<input type="file" name="imagem" multiple="multiple" />
											<input type="submit" value="salvar" id="salvar_form" />
										</div>								
									</form>
									
							  </div>
																						
							
				</div>
			</div>
		</div><!-- end of main_container_adm-->

		<div id="baixo">
			<div class="center_footer">
				Todos os Direitos Reservados - Designed by CSS Creme, editado pelo grupo ( Eduardo, Jhonny & Kleber ).<br /><br />
			</div>
		</div>
	</body>
</html>

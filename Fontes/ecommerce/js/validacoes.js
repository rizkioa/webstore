function validar_form(cadastro) {
	//----------------valida nome---------------
	if(cadastro.primeiro_nome.value == ''){
		alert("Preencha o Primeiro nome");
		return false;
	}
	if(cadastro.ultimo_nome.value == ''){
		alert("Preencha o Ultimo nome");
		return false;
	}
	if(cadastro.usuario.value == ''){
		alert("Preencha o Apelido");
		return false;
	}
	//----------------valida data---------------
		var date=cadastro.data_nascimento.value;
		var ardt=new Array;
		var ExpReg=new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");
		ardt=date.split("/");
		erro=false;
		if ( date.search(ExpReg)==-1){
			erro = true;
		}
		else if (((ardt[1]==4)||(ardt[1]==6)||(ardt[1]==9)||(ardt[1]==11))&&(ardt[0]>30))
			erro = true;
		else if ( ardt[1]==2) {
		if ((ardt[0]>28)&&((ardt[2]%4)!=0))
			erro = true;
		if ((ardt[0]>29)&&((ardt[2]%4)==0))
			erro = true;
		}
		if (erro) {
			alert("Data inválida");
			return false;
		}
		/*cadastro.data_nascimento.value = cadastro.data_nascimento.value.replace('/','-');
		var tmp = cadastro.data_nascimento.value;
		var tmp = ddmmyyyy.split('-');
		cadastro.data_nascimento.value = tmp[2]+'-'+tmp[1]+'-'+tmp[0];*/
	//-----------------fim data-----------------
	//----------------valida CPF----------------
    // Elimina CPF sem 11 digitos:
    var cpf = cadastro.cpf.value.replace(/[^0-9]+/g,'');
    if (cpf.length != 11){
        alert("CPF invalido");
        return false;
	}
    // Valida 1o digito:
    add = 0;
    for (i=0; i < 9; i ++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(9))){
    	alert("CPF invalido");
        return false;
    }
    // Valida 2o digito
    add = 0;
    for (i = 0; i < 10; i ++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(10))){
        alert("CPF inválido");
        return false;
    }
    cadastro.cpf.value = cadastro.cpf.value.replace(/[^0-9]+/g,'');
    //-----------------fim CPF-------------------
	//---------------valida e-mail---------------
	if (cadastro.email.value.indexOf("@")<1 || cadastro.email.value.lastIndexOf(".")<cadastro.email.value.indexOf("@")+2 || cadastro.email.value.lastIndexOf(".")+2 >= cadastro.email.value.length){
		alert("E-mail inválido");
		return false;
	}
	if (cadastro.email.value != cadastro.cemail.value){
		alert("E-mails não conferem!")
		return false;
	}
	//----------------fim e-mail-----------------
	//---------------valida senha----------------
	if (cadastro.senha.value.length < 4){
		alert("Senha muito curta!");
		return false;
	}
	if (cadastro.senha.value != cadastro.csenha.value){
		alert("Senhas não conferem");
		return false;
	}
	//----------------fim senha------------------
	//---------------valida telefone-------------
	
	//----------------fim telefone---------------
	cadastro.email.value = cadastro.email.value.replace(/[^0-9]+/g,'');
	cadastro.telefone.value = cadastro.telefone.value.replace(/[^0-9]+/g,'');
	return true;
}

function validar_contato(contato) {
	//---------------valida e-mail---------------
	if (contato.email.value.indexOf("@")<1 || contato.email.value.lastIndexOf(".")<contato.email.value.indexOf("@")+2 || contato.email.value.lastIndexOf(".")+2 >= contato.email.value.length){
		alert("E-mail inválido");
		return false;
	}
	if (contato.email.value != contato.cemail.value){
		alert("E-mails não conferem!")
		return false;
	}
	//----------------fim e-mail-----------------
	//---------------valida telefone-------------
	contato.telefone.value = contato.telefone.value.replace(/[^0-9]+/g,'');
	//----------------fim telefone-------------
	return true;
}

function valida_data1(data1) {
	var date=data1.inicial1.value;
		var ardt=new Array;
		var ExpReg=new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");
		ardt=date.split("/");
		erro=false;
		if ( date.search(ExpReg)==-1){
			erro = true;
		}
		else if (((ardt[1]==4)||(ardt[1]==6)||(ardt[1]==9)||(ardt[1]==11))&&(ardt[0]>30))
			erro = true;
		else if ( ardt[1]==2) {
		if ((ardt[0]>28)&&((ardt[2]%4)!=0))
			erro = true;
		if ((ardt[0]>29)&&((ardt[2]%4)==0))
			erro = true;
		}
		if (erro) {
			alert("Informe uma data inicial válida");
			return false;
		}
		var date=data1.final1.value;
		var ardt=new Array;
		var ExpReg=new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");
		ardt=date.split("/");
		erro=false;
		if ( date.search(ExpReg)==-1){
			erro = true;
		}
		else if (((ardt[1]==4)||(ardt[1]==6)||(ardt[1]==9)||(ardt[1]==11))&&(ardt[0]>30))
			erro = true;
		else if ( ardt[1]==2) {
		if ((ardt[0]>28)&&((ardt[2]%4)!=0))
			erro = true;
		if ((ardt[0]>29)&&((ardt[2]%4)==0))
			erro = true;
		}
		if (erro) {
			alert("Informe uma data final válida");
			return false;
		}
		
		/*data1.inicial1.value = data1.inicial1.value.replace('/','');
		var tmp = data1.inicial1.value;
		var tmp = ddmmyyyy.split('-');
		data1.inicial1.value = tmp[2]+'-'+tmp[1]+'-'+tmp[0];
		
		data1.final1.value = data1.final1.value.replace('/','');
		var tmp = data1.final1.value;
		var tmp = ddmmyyyy.split('-');
		data1.final1.value = tmp[2]+'-'+tmp[1]+'-'+tmp[0];*/
}

function valida_data2(data2) {
	var date=data2.inicial2.value;
		var ardt=new Array;
		var ExpReg=new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");
		ardt=date.split("/");
		erro=false;
		if ( date.search(ExpReg)==-1){
			erro = true;
		}
		else if (((ardt[1]==4)||(ardt[1]==6)||(ardt[1]==9)||(ardt[1]==11))&&(ardt[0]>30))
			erro = true;
		else if ( ardt[1]==2) {
		if ((ardt[0]>28)&&((ardt[2]%4)!=0))
			erro = true;
		if ((ardt[0]>29)&&((ardt[2]%4)==0))
			erro = true;
		}
		if (erro) {
			alert("Informe uma data inicial válida");
			return false;
		}
		var date=data2.final2.value;
		var ardt=new Array;
		var ExpReg=new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");
		ardt=date.split("/");
		erro=false;
		if ( date.search(ExpReg)==-1){
			erro = true;
		}
		else if (((ardt[1]==4)||(ardt[1]==6)||(ardt[1]==9)||(ardt[1]==11))&&(ardt[0]>30))
			erro = true;
		else if ( ardt[1]==2) {
		if ((ardt[0]>28)&&((ardt[2]%4)!=0))
			erro = true;
		if ((ardt[0]>29)&&((ardt[2]%4)==0))
			erro = true;
		}
		if (erro) {
			alert("Informe uma data final válida");
			return false;
		}
		
		/*data2.inicial2.value = data2.inicial2.value.replace('/','');
		var tmp = data2.inicial2.value;
		var tmp = ddmmyyyy.split('-');
		data2.inicial2.value = tmp[2]+'-'+tmp[1]+'-'+tmp[0];
	
		data2.final2.value = data2.final2.value.replace('/','');
		var tmp = data2.final2.value;
		var tmp = ddmmyyyy.split('-');
		data2.final2.value = tmp[2]+'-'+tmp[1]+'-'+tmp[0];*/
}

function compra(paga){
	var compra = paga.compra1.value;
	if(compra == ''){
		alert("Preencha o número do cartão");
		return false;
	}else{
		compra = paga.compra2.value;
		if(compra == ''){
			alert("Preencha os Nome do titular");
			return false;
		}else{
			compra = paga.compra3.value;
			if(compra == ''){
				alert("Preencha o CPF do titular");
				return false;
			}else{
				compra = paga.compra4.value;
				if(compra == ''){
					alert("Preencha o código de segurança");
					return false;
				}
			}
		}
	}
	alert("Compra efetuada, verificando pagamento");
}

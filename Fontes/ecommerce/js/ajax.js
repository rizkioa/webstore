var xmlhttp;
if(window.XMLHttpRequest){
	xmlhttp = new XMLHttpRequest();
	xmlhttp.overrideMimeType('text/xml');
}
else if(window.ActiveXObject){
	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
}
else{
	alert ("Browser sem suporte para XML HTTP Request! Troque de navegador!");
}

function vendas() {
	var inicial1 = document.getElementById("inicial1").value;
	var final1 = document.getElementById("final1").value;
	var dados = "inicial1="+inicial1+"&final1="+final1;
	xmlhttp.onreadystatechange = function(){
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200 ){
			document.getElementById("fundo_compras").innerHTML = xmlhttp.responseText;
		}
		else{
			return false;
		}
	}
	xmlhttp.open("POST","alimenta_admin_relatorio_vendas.php", true);
	xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("Content-length", dados.length);
	xmlhttp.send(dados);
	document.form_post.reset();
}

function fabricante(){
	var inicial2 = document.getElementById("inicial2").value;
	var final2 = document.getElementById("final2").value;
	var fabric = document.getElementById("fabric").value;
	var dados2 = "inicial2="+inicial2+"&final2="+final2+"&fabric="+fabric;
	xmlhttp.onreadystatechange = function(){
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200 ){
			document.getElementById("fundo_compras2").innerHTML = xmlhttp.responseText;
		}
		else{
			return false;
		}
	}
	xmlhttp.open("POST","alimenta_admin_fabricantes.php", true);
	xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("Content-length", dados2.length);
	xmlhttp.send(dados2);
	document.form_post.reset();	
}

function fidelidade(){
	var cliente = document.getElementById("form_cliente").value;
	var cliente = "cliente="+cliente;
	xmlhttp.onreadystatechange = function(){
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200 ){
			document.getElementById("fundo_compras3").innerHTML = xmlhttp.responseText;
		}
		else{
			return false;
		}
	}
	xmlhttp.open("POST","alimenta_admin_clientesfieis.php", true);
	xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("Content-length", cliente.length);
	xmlhttp.send(cliente);
	document.form_post.reset();	
}

function acao4(){
	var search = document.getElementById("search").value;
	var search = "search="+search;
	//var pag = document.getElementById("submit_busca").value;
	xmlhttp.onreadystatechange = function(){
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200 ){
			document.getElementById("dados").innerHTML = xmlhttp.responseText;
		}
		else{
			return false;
		}
	}
	xmlhttp.open("POST","alimenta_dados_index.php", true);
	xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("Content-length", search.length);
	xmlhttp.send(search);
	document.form_post.reset();
}

function acao5(){
	var search = document.getElementById("search_produto").value;
	var search = "search="+search;

	xmlhttp.onreadystatechange = function(){
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200 ){
			document.getElementById("fundo_produtos1").innerHTML = xmlhttp.responseText;
		}
		else{
			return false;
		}
	}
	xmlhttp.open("POST","alimenta_admin_produtos.php", true);
	xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("Content-length", search.length);
	xmlhttp.send(search);
	document.form_post.reset();	
}

function user(){
	var search = document.getElementById("search_usuario").value;
	var search = "search="+search;

	xmlhttp.onreadystatechange = function(){
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200 ){
			document.getElementById("fundo_usuarios1").innerHTML = xmlhttp.responseText;
		}
		else{
			return false;
		}
	}
	xmlhttp.open("POST","alimenta_admin_users.php", true);
	xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("Content-length", search.length);
	xmlhttp.send(search);
	document.form_post.reset();	
}

$(document).ready(function(){
	$('#login-trigger').click(function(){
		$(this).next('#login-content').slideToggle();
		$(this).toggleClass('active');

		if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
		else $(this).find('span').html('&#x25BC;')
	});
});

$(document).ready(function(){
	$(window).resize(function() {
		if ( parseInt($('.barratop').css("width")) < 710 ){
			$('#login-trigger').css("display", 'none');
		}
		console.log("ele encolheu ele vale " + $('.barratop').css("width"));
	});
});

$(document).ready(function(){
	$('#interior_carrinho').toggle();
	$('#fundo_interior_carrrinho').toggle();
	$('.carrinho').click(function(){
		$('.carrinho').slideDown('slow');
		$('#interior_carrinho').slideToggle();
		$('#fundo_interior_carrrinho').slideToggle();
		if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
		else $(this).find('span').html('&#x25BC;')
	});
});

function initMenu() {
	$('#menu ul').hide();
	$('#menu li a').click(function(){
		$a = $(this).parent().children('ul');
		$('#menu li ul').each(function(){
			$(this).stop().slideUp('normal');
		});
		$(this).next().slideToggle('normal');
	});
}
$(document).ready(function() {initMenu();});
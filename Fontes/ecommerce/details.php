<?php 
	require ('cria_sessao.php'); 
	require ('alimenta_dados_produto.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>WebStore</title>
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/style_produtos_imagens_temp.css">
		<link rel="stylesheet" type="text/css" href="css/style_pagamento_img.css" />
		<?php require ("./includes/js.inc"); ?>
	</head>
	<body>
		
		<?php include ("includes/menu_superior.inc"); ?>
		
		<div id="main_container">
			<div id="presentation">

				<?php
					include ("./includes/slider.inc");
					include ("./includes/login.inc");
				?>
				
			</div>

			<div class="barra_produto">
				<div> DETALHES DO PRODUTO: &nbsp <?= $consulta3[nome] . " - " . $consulta[nome] ?>
					<!-- <a id="carrinho_link" href="carrinho.html"> Minhas Compras </a> --><!-- Botão "Minhas Compras azul na direita -->
					<!-- <div class="carrinho">Meu Carrinho
								<div id="interior_carrinho">
									<div id="fundo_interior_carrrinho"> </div>	
								</div>
							</div> -->
				</div>
			</div>
			<div id="main_content">
				<div class="left_content">
					<?php include ("includes/accordion_menu.inc"); ?>
				</div>	<!-- CLOSE: class="left_content" -->

				<div id="center_content_info">
					<div class="center_prod_box_big">
						<div class="product_img_big">
							<div class="thumbs"> <img id="img_big" src="<?= $imagem[endereco] ?>" /> </div>
						</div>
						<div class="details_big_box">
							<div class="specifications">
								<div class="product_title_big"><?=  $consulta[nome] . " " . $consulta[tecnologia] ?></div>
								<span class="blue"> Código do produto: <?= $consulta[cod_produto] ?> </span>
								<hr width="90%" align="center" style=" border: 1px #F0F4F5 solid; margin-top: 15px;" />
								<span class="black"> Preço: </span> <span class="blue_2"> <?= $valor=(number_format($consulta[valor],2,",",".")) ?> </span> <br />
								<span class="black"> No Boleto: </span> <span class="blue_2"> <?= $boleto=(number_format(($consulta[valor] * 0.9),2,",",".")) ?> </span><span class="blue">(10% de Desconto)</span>
								<br /><br />
								<span id="parcela_text"> ou em até 12x de <?= number_format(($consulta[valor] = $consulta[valor] * 1.10 ) / 12, 2,",",".") ?> no cartão </span> <br />
								<hr width="90%" align="center" style=" border: 1px #F0F4F5 solid; margin-top: 15px;" />
								<hr width="90%" align="center" style=" border: 1px #F0F4F5 solid; margin-top: 15px;" />
								<div><a href="alimenta_carrinho.php?proid=<?=$consulta[cod_produto];?>&page=<?= $lastpage = 'carrinho.php';?>&acao=add" id='fin_compra' > Comprar </a></div>
							</div>
						</div>
					</div>
					<div name="informacoes" id="barra_produto2">
						<span id="info_text"> Informações do Produto </span>
					</div>
					<div id="infos">
						<div class="infos_celula1"> <span class="prod_detalhes1" ><b> Marca </b> </span> </div>
						<div class="infos_celula2"> <span class="prod_detalhes2" ><?= $consulta3[nome] ?>  </span> </div>
						<div class="infos_celula1"> <span class="prod_detalhes1" ><b> Display </b> </span> </div>
						<div class="infos_celula2"> <span class="prod_detalhes2" ><?= $consulta[display] ?> </span> </div>
						<div class="infos_celula1"> <span class="prod_detalhes1" ><b> Tecnologia </b>  </span> </div>
						<div class="infos_celula2"> <span class="prod_detalhes2" ><?= $consulta[tecnologia] ?> </span> </div>
						<div class="infos_celula1"> <span class="prod_detalhes1" ><b> Wi-Fi </b> </span> </div>
						<div class="infos_celula2"> <span class="prod_detalhes2" ><?= $consulta[wifi] ?> </span> </div>
						<div class="infos_celula1"> <span class="prod_detalhes1" ><b> Bluetooth </b> </span> </div>
						<div class="infos_celula2"> <span class="prod_detalhes2" ><?= $consulta[bluetooth] ?> </span> </div>
						<div class="infos_celula1"> <span class="prod_detalhes1" ><b> Cor </b> </span> </div>
						<div class="infos_celula2"> <span class="prod_detalhes2" ><?= $consulta[cor] ?> </span> </div>
						<div class="infos_celula1"> <span class="prod_detalhes1" ><b> Câmera </b> </span> </div>
						<div class="infos_celula2"> <span class="prod_detalhes2" ><?= $consulta[camera_traseira] ?> </span> </div>
						<div class="infos_celula1"> <span class="prod_detalhes1" ><b> Câmera Frontal </b> </span> </div>
						<div class="infos_celula2"> <span class="prod_detalhes2" ><?= $consulta[camera_frontal] ?> </span> </div>
						<div class="infos_celula1"> <span class="prod_detalhes1" ><b> Idiomas </b> </span> </div>
						<div class="infos_celula2"> <span class="prod_detalhes2" ><?= $consulta[idiomas] ?> </span> </div>
						<div class="infos_celula1"> <span class="prod_detalhes1" ><b> Recursos </b> </span> </div>
						<div class="infos_celula2"> <span class="prod_detalhes2" ><?= $consulta[recursos] ?></span></div>
						<div class="infos_celula1"> <span class="prod_detalhes1" ><b> Processador </b> </span> </div>
						<div class="infos_celula2"> <span class="prod_detalhes2" ><?= $consulta[processador] ?> </span> </div>
						<div class="infos_celula1"> <span class="prod_detalhes1" ><b> Peso Liq.(kg) </b> </span> </div>
						<div class="infos_celula2"> <span class="prod_detalhes2" ><?= $consulta[peso_liquido] . "g" ?> </span> </div>
						<div class="infos_celula1"> <span class="prod_detalhes1" ><b> Modelo </b> </span> </div>
						<div class="infos_celula2"> <span class="prod_detalhes2" ><?= $consulta[modelo] ?> </span> </div>
						<div class="infos_celula1"> <span class="prod_detalhes1" ><b> Garantia </b> </span> </div>
						<div class="infos_celula2"> <span class="prod_detalhes2" ><?= $consulta[garantia] . " meses" ?> </span> </div>
					</div>
				</div>	<!-- CLOSE: class="center_content_info" -->
				
				
			</div>	<!-- CLOSE: id="main_content" -->
			<div class="footer"></div> <!-- NÃO DELETAR PORQUE CARREGA CSS -->
		</div>	<!-- CLOSE: id="main_container" -->

		<?php include ("includes/footer.inc"); ?>
	</body>
</html>

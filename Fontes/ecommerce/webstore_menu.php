<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Webstore extends CI_Controller {
 
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}
	 
	public function index()
	{
	    $this->load->view('webstore');
	}
	
	public function cadastro()
	{
		$this->load->view('cadastro');
	}
	
	public function produtos()
	{
		$this->load->view('produtos');
	}
	
	public function contato()
	{
		$this->load->view('contato');
	}
	
	public function salva_cliente()
	{
		$this->load->view('salva_cliente_no_banco');
	}
	
	public function ofertas()
	{
		$this->load->view('ofertas');
	}
	
	public function meu_cadastro()
	{
		$this->load->view('conta_cliente');
	}
	public function login_valida()
	{
		$this->load->view('valida_login');
	}
}
<?php require ('cria_sessao.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="./js/accordion.js" > </script>
		<title> WebStore | Admin</title>
		<link rel="stylesheet" type="text/css" href="./css/style.css" />
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<link rel="stylesheet" href="/resources/demos/style.css" />
		<link rel="stylesheet" type="text/css" href="./css/style.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_admin.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_admin_relat.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_pagamento_img.css" />
		<link rel="stylesheet" type="text/css" href="./css/style_produtos_imagens_temp.css" />
		<script type="text/javascript" src="./js/ajax.js"></script>
		<link rel="shortcut icon" href="./images/shoppingcart.png" />
		<script type="text/javascript" src="./js/validacoes.js"></script>
		<script type="text/javascript" src="./js/jquery.maskedinput.min.js"></script>
		<script>
			$(function() {
				$("#date").mask("99/99/9999");
				$("#cep").mask("99.999-999");
				$("#cpf").mask("999.999.999-99");
				$("#telefone").mask("(99)9999-9999?9");
				$("#numero").mask("9?99999");
			});
			$(function() {
				$( "#tabs" ).tabs();
			});
		</script>
	</head>
	<body>
		<div id="barratop_adm">
			<img id="img_adm" src="./images/admin.png"/>
			<div id="title_adm"> WebStore Admin</div>
		</div>
		
		<?php include ("includes/menu_adm.inc"); ?>

		<div id="main_container_adm">
			<div id="main_content">
				<div id="tabs">
					<ul>
						<li><a href="#tabs-1" class="menu_select"> Clientes | Geral </a></li>
						<li><a href="#tabs-2" class="menu_select"> Adicionar </a></li>
					</ul>
					<div id="tabs-1">
						<div id="fundo_admin">
							<div id="grid_search">
								<div id="grid_search_relat">
									<div class="search_data">Pesquisar Clientes</div>
									<form method="POST" name="form_post" id="form_post" >
										<div class="search_data">
											<input name="search_usuario" id="search_usuario" type="text" class="cadastro_input_medium" />
										</div>
										<div class="search_data">
											<button type="button" onclick="user()" class="search_buttom_crud" id="pesquisar"/>Pesquisar</button>
										</div>
									</form>
								</div>
							</div>
							<div id="grid_menu_compras">
								<div id="grid_menu_compras1"> <span class="sub_menu_compras"> Cod - Nome </span> </div>
								<div id="grid_menu_users2"> <span class="sub_menu_compras"> UF </span> </div>
								<div id="grid_menu_users3"> <span class="sub_menu_compras1"> Total Compras </span> </div>
								<div id="grid_menu_compras4"> <span class="sub_menu_compras2"> Compras R$ </span> </div>
							</div>
							<div class="fundo_compras" id="fundo_usuarios1">
								
								<?php include ('alimenta_admin_users.php'); ?>
								
							</div>
						</div>
					</div>
					<div id="tabs-2">
						<div id="fundo_admin">
							<div id="fundo_admin2">
								<div class="barra_add_remove_admin"> Adicionar usuário </div>
								<form name="cadastro" action="salva_cliente_no_banco.php?page=<?= $lastpage = basename($_SERVER['PHP_SELF']);?>" method="post" onsubmit='return validar_form(this);'>
									<div class="form_row">
										<label class="cadastro"><strong>Administrador:</strong></label>
										<input type="checkbox" name="admin" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Primeiro Nome:</strong></label>
										<input type="text" name="primeiro_nome" class="cadastro_input" maxlength="30"/>
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Último Nome:</strong></label>
										<input type="text" name="ultimo_nome" class="cadastro_input" maxlength="30"/>
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Apelido:</strong></label>
										<input type="text" name="usuario" class="cadastro_input" maxlength="30"/>
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Data de Nascimento:</strong></label>
										<input type="text" name="data_nascimento" id="date" class="datepicker">
										<label id="cadast_sex"><strong>Sexo:</strong></label>
											<label class="cadastro_sex"> <strong>M</strong> </label>
											<input type="radio" name="sexo" value="M">
											<label class="cadastro_sex"> <strong>F</strong> </label>
											<input type="radio" name="sexo" value="F">
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>CPF:</strong></label>
										<input type="text" id="cpf" name="cpf" class="cadastro_input" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Endereço:</strong></label>
										<input type="text" name="logradouro" class="cadastro_input_medium" maxlength="30"/>
										<label class="cadastro_short"><strong>N.º:</strong></label>
										<input type="text" name="numero"  id="numero" class="cadastro_input_short" maxlength="4"/>
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Complemento:</strong></label>
										<input type="text" name="complemento" class="cadastro_input_medium" maxlength="30"/>
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Bairro:</strong></label>
										<input type="text" name="bairro" class="cadastro_input_medium" />
									</div>	
									<div class="form_row">
										<label class="cadastro"><strong>CEP:</strong></label>
										<input type="text" id="cep" name="cep" class="cadastro_input_medium" />
									</div>
									<div class="form_row">
										<?php include ('./includes/busca_cidade.php'); ?>
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Telefone:</strong></label>
										<input type="text" name="telefone" id="telefone" class="cadastro_input" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>E-mail:</strong></label>
										<input type="email" name="semail" class="cadastro_input" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Confirmar E-mail:</strong></label>
										<input type="email" name="cemail" class="cadastro_input" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Senha:</strong></label>
										<input type="password" name="senha" class="cadastro_input" />
									</div>
									<div class="form_row">
										<label class="cadastro"><strong>Confirmar Senha:</strong></label>
										<input type="password" name="csenha" class="cadastro_input" />
									</div>
									<div class="form_row">
										<input type="submit" name="salvar" id="enviar" value="Salvar" />
									</div>
								</form>
							</div>
						</div>
							
						</div>
					</div>
				</div>
			</div>
		</div><!-- end of main_container_adm-->

		<div id="baixo">
			<div class="center_footer">
				Todos os Direitos Reservados - Designed by CSS Creme, editado pelo grupo ( Eduardo, Jhonny & Kleber ).<br /><br />
			</div>
		</div>
	</body>
</html>

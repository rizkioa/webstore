<script type="text/javascript" src="./js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="./js/accordion.js"></script>
<script type="text/javascript" src="./js/jslider.js"></script>
<script type="text/javascript" src="./js/validacoes.js"></script>
<script type="text/javascript" src="./js/jquery.maskedinput.min.js"></script>
<script type="text/javascript">
	$(function() {
		$("#date").mask("99/99/9999");
		$("#cep").mask("99.999-999");
		$("#cpf").mask("999.999.999-99");
		$("#telefone").mask("(99)9999-9999?9");
		$("#numero").mask("9?99999");
		$("#cod_seg").mask("99");
	});
	
	$(function() {
		$(".slider").jslider({
			btnNext: ".next",
			btnPrev: ".prev"
		})
	});
</script>

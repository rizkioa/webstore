#!/bin/bash

ARGS=2;
USER=$1;
SQL_FILE=$2;
DB='ecommerce';
MYSQL=`which mysql`

if [ $# -ne $ARGS ]
then
	echo "Especificar os argumentos (usuario e script SQL)!";
	echo "Uso: $0 usuario script_MySQL.sql";
	exit;
fi

echo "Usuario: $USER";
echo "DB: $DB";
echo "Script: $SQL_FILE";

SQL="DROP DATABASE ${DB} IF EXISTS; CREATE DATABASE ${DB} CHARACTER SET utf8 COLLATE utf8_unicode_ci;"

echo "Fornecer sua senha de usuario $USER para o MySQL quando solicitado:"
$MYSQL -u $USER -p -e "$SQL";
$MYSQL -u $USER -p -D $DB < $SQL_FILE;

echo "Banco $DB criado pelo usuario $USER";
exit;

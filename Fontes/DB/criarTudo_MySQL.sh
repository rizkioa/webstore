#!/bin/bash

ARGS=3;
USER=$1;
PASS=$2;
SQL_FILE=$3;
DB='ecommerce';
MYSQL=`which mysql`

if [ $# -ne $ARGS ]
then
	echo "Especificar os 3 argumentos abaixo!";
	echo "Uso: $0 usuario senha script_MySQL.sql";
	exit;
fi

echo "Usuario: $USER";
echo "DB: $DB";
echo "Script: $SQL_FILE";

SQL="CREATE USER '${USER}'@'localhost';
SET PASSWORD FOR '${USER}'@'localhost' = password('${PASS}');
CREATE DATABASE IF NOT EXISTS ${DB} CHARACTER SET utf8 COLLATE utf8_unicode_ci;
GRANT ALL PRIVILEGES ON ${DB}.* TO '${USER}'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;"

echo "Fornecer a senha de root para o MySQL quando solicitado:"
$MYSQL -u root -p -e "$SQL";
$MYSQL -D $DB -u $USER -p$PASS < $SQL_FILE;

echo "Banco $DB criado pelo usuario $USER";
exit;
